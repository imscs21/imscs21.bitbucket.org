<?php
$lang = array(
	'home_info' => '첫 화면으로 이동 합니다' ,
	'install_warning' => '현재 설치된 GR Board <sup>2</sup> 는 정식 버전이 아닌 개발자용 테스트 버전 입니다. 테스트에 도움을 주셔서 감사 드립니다.' ,
	'install_db_type' => 'GR Board <sup>2</sup> 는 현재 MySQL 만 지원 되며, 추후 MariaDB 가 지원될 예정 입니다.' ,
	'install_welcome' => '<strong>환영 합니다!</strong><p>GR Board <sup>2</sup> 는 작고 빠른 설치형 게시판 엔진 입니다. 2005년 10월 국내에선 거의 처음으로 웹표준 기반으로 제작 되었으며,' .
							' 2014년 1월 v2.0.0 이후 부터는 완전한 HTML5 기반으로 새롭게 재작성 되어 배포 되고 있습니다. 주요 특징은 아래와 같습니다.</p>' ,
	'install_feature' => '<ul><li>HTML5 기반으로 작성된 오픈소스 게시판 엔진</li>' .
							'<li>Apache 2 mod_rewrite 를 이용한 단순하고 강력한 URL 포맷팅</li>' .
							'<li>빠르게 동작하며 서버 부하를 최소화 하도록 설계됨</li></ul>' ,
	'install_license' => '라이센스는 GPL v2 (<a href="http://www.gnu.org/licenses/gpl-2.0.html">' . 
							'http://www.gnu.org/licenses/gpl-2.0.html</a>) 를 사용 하고 있습니다.' ,
	'install_notice' => '설치를 계속 진행 하기 위해 아래의 항목들을 빠짐없이 입력 하신 후 Submit 버튼을 클릭 하셔야 합니다. ' .
							'설치를 계속 하시면 위의 라이센스에 동의 하신 것으로 간주 됩니다.' ,
	'install_hostname' => 'DB 호스트네임을 입력해 주세요. (모를 시 서버 관리자/웹호스팅 업체에 문의 필요)' ,
	'install_dbusername' => 'DB 계정 ID 를 입력해 주세요. (모를 시 서버 관리자/웹호스팅 업체에 문의 필요)' ,
	'install_dbpasswd' => 'DB 계정 비밀번호를 입력해 주세요. (모를 시 서버 관리자/웹호스팅 업체에 문의 필요)' ,
	'install_dbname' => '사용할 DB 이름을 입력해 주세요. (보통 계정 ID 와 동일)' ,
	'install_dbprefix' => '중복 설치 시 필요한 테이블 구분자 지정 (기본값 권장)' ,
	'install_admin_info' => 'GR Board 관리자 아이디와 비밀번호를 지정해 주세요.'
);
?>