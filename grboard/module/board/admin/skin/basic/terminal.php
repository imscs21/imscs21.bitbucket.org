<?php if(!defined('GR_BOARD_2')) exit(); ?>

<h2>Terminal <sup class="text-danger">α</sup></h2>
<div id="cmdBox">
	<div id="cmdLog" contenteditable="true" class="well"></div>
	<input type="text" id="cmdLineInput" class="form-control" placeholder="Input the command if you want. (ex: help, version, history, status, ...)" />
</div>